//
//  OrderItemsView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 27.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderItemsView : UITableView, UITableViewDataSource{

    private let cellId = "orderItemCell"
    private let itemHeight = 90
    
    var items : [ItemArgo] = []{
        didSet{
            dispatch_async(dispatch_get_main_queue(), { [weak self] in
                self!.reloadData()
            })
        }
    }
    
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        dataSource = self
        rowHeight = CGFloat(itemHeight)
        registerClass(OrderItemCell.self, forCellReuseIdentifier: cellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.snp_updateConstraints(closure: {(make) in
            var height = (itemHeight * items.count)
            
            if height > 270{
                height = 270
            }
            
            make.height.equalTo(height)
        })
        
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! OrderItemCell
        
        let item = items[indexPath.row]
        
        cell.setContent(item.img, itemName: item.name, itemCount: item.count, price: item.priceVat)
        
        return cell
    }
    
}