//
//  OrderNumberBoxView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 28.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderNumberBoxView : UIView{
    
    var orderNumber : String?{
        didSet{
            if let number = orderNumber{
                orderNumberValueLabel.text = number
            }
        }
    }
    
    private let orderNumberValueLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 236/255, green: 245/255, blue: 254/255, alpha: 1)
        
        let orderNumberLabel = UILabel()
        orderNumberLabel.text = "Objednávka:"
        orderNumberLabel.font = UIFont(name: "NimbusSanL-Bol", size: 18)
        
        orderNumberValueLabel.font = UIFont(name: "NimbusSanL-Bol", size: 18)
        
        addSubview(orderNumberLabel)
        addSubview(orderNumberValueLabel)
        
        orderNumberLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self).offset(8)
            make.centerY.equalTo(self)
        })
        
        orderNumberValueLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(orderNumberLabel.snp_right).offset(3)
            make.centerY.equalTo(orderNumberLabel)
        })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}