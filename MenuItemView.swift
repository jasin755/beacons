//
//  MenuItemView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class MenuItemView : UITableViewCell{
    
    var title : String? {
        didSet {
            label.text = title
        }
    }
    
    var buttonImage : UIImage? {
        didSet{
            buttonImageView.image = buttonImage
        }
    }
    
    var type : MenuItemType?
    
    
    private var label : UILabel!
    private var buttonImageView : UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let border = UIView()
        buttonImageView = UIImageView()
        label = UILabel()
        
        selectionStyle = UITableViewCellSelectionStyle.None
        
        
        border.backgroundColor = Config.defaultBorderColor
        
        label.font = Config.defaultFont
        label.textColor = Config.defaultFontColor
        
        contentView.addSubview(border)
        contentView.addSubview(buttonImageView)
        contentView.addSubview(label)
        
        border.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(contentView).inset(30)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
            make.height.equalTo(1)
        })
        
        buttonImageView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(contentView).inset(30)
            make.centerY.equalTo(contentView)
        })
        
        
        label.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(buttonImageView.snp_right).offset(20)
            make.centerY.equalTo(contentView)
        })
        
        
    }
    
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
