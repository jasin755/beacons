//
//  Beacons.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 15.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry

struct BeaconArgo {
    let beaonID: String
    let latitude: Double?
    let longitude: Double?
    let radius: Double?
    let targetID: Int
    let targetType: Int
    let messageText: String?
    let messageHeader: String?
    let isSilent: Bool?
}

extension BeaconArgo: Decodable {
    static func decode(j: JSON) -> Decoded<BeaconArgo> {
        return curry(BeaconArgo.init)
        <^> j <| "beaconId"
        <*> j <|? "latitude"
        <*> j <|? "longitude"
        <*> j <|? "radius"
        <*> j <| "targetId"
        <*> j <| "targetType"
        <*> j <|? "messageText"
        <*> j <|? "messageHeader"
        <*> j <|? "isSilent"
    }
}

