//
//  User.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 15.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry
import RealmSwift

struct LoginUser {
    let userName: String
    let error: Int
    let message: String
}

extension LoginUser: Decodable {
    static func decode(j: JSON) -> Decoded<LoginUser> {
        return curry(LoginUser.init)
        <^> j <| "user_name"
        <*> j <| "err"
        <*> j <| "msg"
    }
}

class UserRealm: Object {
    dynamic var name = ""
    dynamic var UIDX = ""

    convenience init(name: String, UIDX: String) {
        self.init()
        self.name = name
        self.UIDX = UIDX
    }
}

class User {
    static func name() -> String {
        let realm = try! Realm()
        let users = realm.objects(UserRealm)
        if users.count > 0 {
            return users[0].name
        }
        return ""
    }

    static func UIDX() -> String {
        let realm = try! Realm()
        let users = realm.objects(UserRealm)
        if users.count > 0 {
            return users[0].UIDX
        }
        return ""
    }
}

