//
//  BeaconHistoryEntity.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 02.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import RealmSwift

class BeaconHistoryEntity: Object {
    
    dynamic var uuid : String = ""
    dynamic var major : Int = 0
    dynamic var minor : Int = 0
    dynamic var identifier : String = ""
    dynamic var date : NSDate = NSDate()

}
