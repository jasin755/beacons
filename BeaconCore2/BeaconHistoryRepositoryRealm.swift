//
//  BeaconHistoryRepositoryRealm.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 02.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import RealmSwift

class BeaconHistoryRepositoryRealm{
    
    private var realm : Realm
    private var notificationToken : NotificationToken!
    
    var beaconHistories : Results<BeaconHistoryEntity>
    var delegate : BeaconHistoryRepositoryDelegate?
    
    init(){
        realm = try! Realm()
        beaconHistories = realm.objects(BeaconHistoryEntity).sorted("date", ascending: true)
        
        notificationToken = beaconHistories.addNotificationBlock{[weak self] (changes: RealmCollectionChange) in
            
            switch changes{
            case .Update(_, deletions: let deletions, insertions: let insertions, modifications: let modifications):
                
                if let delegate = self?.delegate{
                    
                    var insertedBeaconHistories = [Int : BeaconHistoryEntity]()
                    var modificatedBeaconHistories = [Int : BeaconHistoryEntity]()
                    
                
                    if insertions.count > 0{
                        insertions.forEach({(index) in
                            insertedBeaconHistories[index] = self!.beaconHistories[index]
                        })
                    }
                    
                    if modifications.count > 0{
                        modifications.forEach({(index) in
                            modificatedBeaconHistories[index] = self!.beaconHistories[index]
                        })
                    }
                    
                    if insertedBeaconHistories.count > 0 || modificatedBeaconHistories.count > 0 || deletions.count > 0{
                        delegate.recordChanged(insertedBeaconHistories, updated: modificatedBeaconHistories, deletedIndexes: deletions)
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    deinit{
        notificationToken.stop()
    }
    
    func deleteAll() {
        try! realm.write({
            realm.delete(beaconHistories)
        })
    }
    
    func delete(beaconHistoryEntity : BeaconHistoryEntity) {
        try! realm.write({
            realm.delete(beaconHistoryEntity)
        })
    }
    
    
    func saveBeaconRegionHistory(region: CLBeaconRegion) {
        let beaconHistoryEntity = BeaconHistoryEntity()
        beaconHistoryEntity.uuid = region.proximityUUID.UUIDString
        beaconHistoryEntity.major = (region.major?.integerValue)!
        beaconHistoryEntity.minor = (region.minor?.integerValue)!
        beaconHistoryEntity.identifier = region.identifier
        beaconHistoryEntity.date = NSDate()
        
        try! realm.write({
            realm.add(beaconHistoryEntity)
        })
        
    }
    
}

protocol BeaconHistoryRepositoryDelegate {
    func recordChanged(inserted : [Int : BeaconHistoryEntity], updated: [Int : BeaconHistoryEntity], deletedIndexes: [Int])
}