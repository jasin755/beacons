//
//  File.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 22.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry

struct ErrorArgo {
    let id: Int
    let message: String?
    let beacons: [BeaconArgo]?
}

extension ErrorArgo: Decodable {
    static func decode(j: JSON) -> Decoded<ErrorArgo> {
        return curry(ErrorArgo.init)
        <^> j <| "err"
        <*> j <|? "msg"
        <*> j <||? "beacons"
    }
}
