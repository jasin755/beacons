//
//  BeaconRegionHistory.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 01.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import CoreData

@objc(BeaconRegionHistory)
class BeaconRegionHistory : NSManagedObject{
    
    @NSManaged var date : NSDate
    @NSManaged var uuid : String
    @NSManaged var major : NSNumber
    @NSManaged var minor : NSNumber
    @NSManaged var identifier : String
    
}
