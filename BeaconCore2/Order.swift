//
//  Order.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry
import RealmSwift

struct OrderArgo {
    let id: String
    let created: String
    let user: String?
    let email: String
    let phone: String
    let address: String
    let deliveryAdress: String?
    let deliveryNote: String?
    let delivery: String?
    let payment: String?
    let price: String
    let priceVat: String
    let vat: String
    let message: String?
    let messageStatus: String?
    let itemstatusDecription: String?
    let paymentStatusDescription: String?
    let itemsCnt: Int
    let items: [ItemArgo]
}

extension OrderArgo : Decodable{
    static func decode(j: JSON) -> Decoded<OrderArgo> {
        
        let w = curry(OrderArgo.init)
            <^> j <| "id"
            <*> j <| "created"
            <*> j <|? "user"
            <*> j <| "email"
            <*> j <| "phone"
            <*> j <| "address"
            <*> j <|? "deliveryAddress"
            <*> j <|? "deliveryNote"
        
        let t = w
            <*> j <|? "delivery"
            <*> j <|? "payment"
            <*> j <| "Price"
            <*> j <| "PriceVat"
            <*> j <| "Vat"
            <*> j <|? "message"
            <*> j <|? "messageStatus"
            <*> j <|? "itemstatusdescription"

        
        let f = t
            <*> j <|? "paymentstatusdescription"
            <*> j <| "items_cnt"
            <*> j <|| "items"
        
        return f
        
    }
}


class OrderRealm : Object{
    var id: String!
    var created: String!
    var user: String?
    var email: String!
    var phone: String!
    var address: String!
    var deliveryAdress: String?
    var deliveryNote: String?
    var delivery: String?
    var payment: String?
    var price: String!
    var priveVat: String!
    var vat: String!
    var message: String?
    var messageStatus: String?
    var itemstatusDecription: String?
    var paymentStatusDescription: String?
    var itemsCnt: Int = 0
    var items = List<ItemRealm>()
    
    convenience init(id : String, created : String, user : String?, email : String, phone : String, address : String, deliveryAdress: String?, deliveryNote: String?, delivery: String?, payment: String?, price: String, priveVat: String, vat: String, message: String?, messageStatus: String?, itemstatusDecription: String?, paymentStatusDescription: String?, itemsCnt: Int){
        
        self.init()
        
        self.id = id
        self.created  = created
        self.user = user
        self.email = email
        self.phone = phone
        self.address = address
        self.deliveryAdress = deliveryAdress
        self.deliveryNote = deliveryNote
        self.delivery = delivery
        self.payment = payment
        self.price = price
        self.priveVat = priveVat
        self.vat = vat
        self.message = message
        self.messageStatus = messageStatus
        self.itemstatusDecription = itemstatusDecription
        self.paymentStatusDescription = paymentStatusDescription
        self.itemsCnt = itemsCnt
    }
    
}

struct ItemArgo {
    let order: Int
    let code: String
    let name: String
    let count: Int
    let priceVat: String
    let priceItemVat: String
    let img: String?
}

extension ItemArgo : Decodable{
    static func decode(j: JSON) -> Decoded<ItemArgo> {
        return curry(ItemArgo.init)
            <^> j <| "order"
            <*> j <| "code"
            <*> j <| "name"
            <*> j <| "count"
            <*> j <| "price_vat"
            <*> j <| "price_item_vat"
            <*> j <|? "img"
    }
}

class ItemRealm : Object{
    dynamic var order: Int = 0
    dynamic var code: String = ""
    dynamic var name: String = ""
    dynamic var count: Int = 0
    dynamic var priceVat: String = ""
    dynamic var priceItemVat: String = ""
    dynamic var img: String = ""
    dynamic var orderRealm : OrderRealm?
    
    convenience init(order: Int, code: String, name: String, count: Int, priceVat: String, priceItemVat: String, img: String?){
        
        self.init()
        
        self.order = order
        self.code = code
        self.name = name
        self.count = count
        self.priceVat = priceVat
        self.priceItemVat = priceItemVat
        
        if let i = img{
            self.img = i
        }
    }

}
