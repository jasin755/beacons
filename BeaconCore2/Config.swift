//
//  Config.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

struct Config{

    static let defaultFont : UIFont = UIFont(name: "NimbusSanL-Reg", size: 16)!
    static let defaultFontColor = UIColor(red: 55/255,green: 55/255, blue: 55/255, alpha: 1)
    static let defaultBorderColor = UIColor(red: 235/255, green: 249/255, blue: 255/255, alpha: 1)
    static let beaconHistoryLength = 5
    
}