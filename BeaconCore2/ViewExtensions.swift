import Foundation
import UIKit

extension UITableViewCell{
   
    func addDefaultBorder(){
        let border = UIView()
        border.backgroundColor = Config.defaultBorderColor
        
        contentView.addSubview(border)
        
        border.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
            make.height.equalTo(1)
        })
        
    }
    
}


extension UIView{
    
    func createLabel(title : String?) -> UILabel{
        let label = UILabel()
        label.text = title
        label.font = Config.defaultFont
        label.textColor = Config.defaultFontColor
        
        if(self is UITableViewCell){
           (self as! UITableViewCell).contentView.addSubview(label)
        }else{
            addSubview(label)
        }
        
        return label
    }
    
}