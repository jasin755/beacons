//
//  MainController.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 23.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MainController : UIViewController, BeaconManagerDelegate{
    
    private var mainView : MainView!
    private var menuView : MenuView!
    private var menuGestureRecognizer : UITapGestureRecognizer?
    private let leftMargin : CGFloat = 180.0
    
    
    private let beaconManager = BeaconManager(historyLength: Config.beaconHistoryLength)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        beaconManager.delegates.append(self)
        
        view.backgroundColor = .whiteColor()
    
        mainView = MainView()
        menuView = MenuView()
        
        view.addSubview(menuView)
        view.addSubview(mainView)
        
        mainView.snp_makeConstraints(closure: {(make) in
            make.edges.equalTo(view)
        })
        
        mainView.menuButtonClick = { [weak self] (button) in
            if self!.menuView.hidden == true{
                self!.showMenu()
            }else{
                self!.hideMenu()
            }
        }
        
        menuView.alpha = 0
        menuView.hidden = true
        
        menuView.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(view)
            make.bottom.equalTo(view)
            make.left.equalTo(view)
            make.right.equalTo(view).inset(123)
        })
        
        menuView.menuClicked = {[weak self] (menuItemtype) in
            
            self!.hideMenu()
            
            switch menuItemtype {
            case MenuItemType.BeaconHistory:
                self!.showBeaconHistory()
                break
            case MenuItemType.HomePage:
                self!.showHomePage()
                break
            case MenuItemType.LastLarticle:
                self!.showLastArticle()
                break
            case MenuItemType.LastOrder:
                self!.showLastOrder()
                break;
            default: break
            }
        }
        
        NetworkManager.loginUser("codecamp.alza@gmail.com", password: "codecamp2016") { [weak self] error in
            if error.id == 0 {
                self!.startMonitoringBeacons()
            } else {
                if let message = error.message {
                    print(message)
                }
            }
        }
        
        if(mainView.contentView == nil){
            showHomePage()
        }
        
    }
        
    
    func showBeaconHistory(){
        let beaconHistoryView = BeaconHistoryView()
        beaconHistoryView.beaconManager = beaconManager
        mainView.contentView = beaconHistoryView
    }
    
    func showHomePage(){
        let homePageView = HomePageView()
        homePageView.beaconManager = beaconManager
        mainView.contentView = homePageView
    }
    
    func showArticle(articleId : Int){
        let articleView = ArticleView()
        articleView.articleId = articleId
        mainView.contentView = articleView
    }
    
    func showLastArticle(){
        let lastArticleView = LastArticleView()
        mainView.contentView = lastArticleView
    }
    
    func showOrder(orderId: String){
        let orderView = OrderView()
        orderView.orderId = orderId
        mainView.contentView = orderView
    }
    
    func showLastOrder(){
        let lastOrderView = LastOrderView()
        mainView.contentView = lastOrderView
    }
    
    
    func showMenu(){
        
        if(menuView.hidden == false){
            return
        }
        
        UIView.animateWithDuration(0.4, animations: { [weak self] in
            self!.mainView.frame.origin.x += self!.leftMargin
            var rotationAndPerspectiveTransform : CATransform3D = CATransform3DIdentity;
            rotationAndPerspectiveTransform.m34 = 1.0 / 900;
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, CGFloat(45.0) * CGFloat(M_PI) / CGFloat(180.0), CGFloat(0.0), CGFloat(1.0), CGFloat(0.0));
            self!.mainView.layer.transform = rotationAndPerspectiveTransform
        })
        
        UIView.animateWithDuration(0.4, animations: { [weak self] in
            self!.menuView.alpha = 1
        })
    
        menuView.hidden = false
        menuGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideMenu))
        mainView.addGestureRecognizer(menuGestureRecognizer!)
    }
    
    func hideMenu(){
        
        if(menuView.hidden == true){
            return
        }
        
        mainView.removeGestureRecognizer(menuGestureRecognizer!)
      
        UIView.animateWithDuration(0.4, animations: { [weak self] in
            self!.menuView.alpha = 0
            }, completion: {[weak self] (finished) in
                self!.menuView.hidden = true
                
        })
        
        UIView.animateWithDuration(0.4, animations: { [weak self] in
            let rotationAndPerspectiveTransform : CATransform3D = CATransform3DIdentity;
            self!.mainView.layer.transform = rotationAndPerspectiveTransform
            self!.mainView.frame.origin.x -= self!.leftMargin
        })
    
    }
    
    private func startMonitoringBeacons() {
        // Vlastni testovaci beacony
        var beaconIds = [
            BeaconID(UUIDString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e", major: 5000, minor: 6), //Objednavky
            BeaconID(UUIDString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e", major: 5000, minor: 5), //Homepage
            BeaconID(UUIDString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e", major: 50277, minor: 45335) //Clanek
        ]
        
        NetworkManager.getBeacons() { [weak self] beacons, error in
            if error.id == 0 {
                beacons.forEach({ (beacon) in
                    
                    // Protoze proste API
                    if (beacon.beaonID.characters.count == 46) {
                        let UUID = NSUUID(UUIDString: beacon.beaonID[beacon.beaonID.startIndex ..< beacon.beaonID.startIndex.advancedBy(36)])
                        
                        if (UUID != nil) {
                            let major = UInt16(beacon.beaonID[beacon.beaonID.startIndex.advancedBy(36) ..< beacon.beaonID.startIndex.advancedBy(41)])!
                            let minor = UInt16(beacon.beaonID[beacon.beaonID.startIndex.advancedBy(41) ..< beacon.beaonID.startIndex.advancedBy(46)])!
                            
                
                            if(beacon.longitude != nil && beacon.latitude != nil && beacon.radius != nil){
                                let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: beacon.latitude!, longitude: beacon.longitude!), radius: CLLocationDistance(beacon.radius!), identifier: beacon.beaonID.uppercaseString)
                                
                                // Pro testovani
                                //beaconIds.append(BeaconID(proximityUUID: UUID!, major: CLBeaconMajorValue(major), minor: CLBeaconMinorValue(minor), region: region))
                            }else{
                                // Pro testovani
                                //beaconIds.append(BeaconID(proximityUUID: UUID!, major: CLBeaconMajorValue(major), minor: CLBeaconMinorValue(minor)))
                            }

                        }
                    }
                })
            } else {
                //                Error!
                print(error.id)
                if let message = error.message {
                    print(message)
                }
            }
            self!.beaconManager.startMonitoring(beaconIds)
        }
    }
    
    
    private func constructBeaconId(beaconRegion : CLBeaconRegion) -> String{
        var beaconId = beaconRegion.proximityUUID.UUIDString
        
        if let major = beaconRegion.major{
            beaconId += "\(major)"
        }
        
        if let minor = beaconRegion.minor{
            beaconId += "\(minor)"
        }
        
        
        
        //TODO: Pro testovani
        if(beaconId == "f7826da6-4fa2-4e98-8024-bc5b71e0893e5027745335".uppercaseString){
            beaconId = "0xf7826da6bc5b71e0893e0x7951686e6c76"
        }else if(beaconId == "f7826da6-4fa2-4e98-8024-bc5b71e0893e50005".uppercaseString){
            beaconId = "f7826da6-4fa2-4e98-8024-bc5b71e0893e3657142817"
        }else{
            beaconId = "f7826da64fa24e988024bc5b71e0893e3988663588"
        }
        
        return beaconId
    }
    
    func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion) {
        let beaconId = constructBeaconId(region)
        
        NetworkManager.getBeaconDetail(beaconId, completion: {(beaconDetail, error) in
            
            if error.id == 0 {
                
                var alertBody : String
                var userInfo : [NSObject : AnyObject]
                var closure : (() -> Void)? = nil
                
                //TODO: Kvuli testovani objednavky
                var targetType = beaconDetail.targetType
                var targetId = beaconDetail.targetID
                if(beaconId == "f7826da64fa24e988024bc5b71e0893e3988663588"){
                    targetType = 5
                    targetId = 158988653
                }
                
                
                switch targetType {
                case 0:
                    alertBody = "Nalezený nový beacon"
                    userInfo = ["targetType" : 0, "targetId" : targetId]
                    closure = {[weak self] in
                        self!.showHomePage()
                    }
                    break
                case 4:
                    alertBody = "Nový článek"
                    userInfo = ["targetType" : 4, "targetId" : targetId]
                    closure = {[weak self] in
                        self!.showArticle(targetId)
                    }
                    break
                case 5:
                    alertBody = "Objednávka č. \(beaconDetail.targetID) je připravena"
                    userInfo = ["targetType" : 5, "targetId" : "\(targetId)"]
                    closure = {[weak self] in
                        self!.showOrder("\(targetId)")
                    }
                    break
                default:
                    print("Neznamy targetType \(beaconDetail.targetType)")
                    return
                }
         
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                if appDelegate.useWhisper == false{
                    let notification: UILocalNotification = UILocalNotification()
                    notification.soundName = UILocalNotificationDefaultSoundName
                    notification.alertBody = alertBody
                    notification.userInfo = userInfo
                    UIApplication.sharedApplication().presentLocalNotificationNow(notification)
                }else{
                    Whisper(title: alertBody, subTitle: nil, controller: self, onClick: closure)
                }
                
            }else{
                if let message = error.message{
                    print(message)
                }else{
                    print("Nastala chyba pri ziskavani detailu beaconu")
                }
            }
        })
        
    }
    
}