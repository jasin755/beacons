//
//  NetworkManager.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 15.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import Alamofire
import Argo
import RealmSwift

class NetworkManager {
    // MARK: User
    static func loginUser(login: String, password: String, completion: (error: ErrorArgo) -> Void) {
        loginUserToServer(login, password: password) { json, UIDX in
            parseLoginUser(json) { loginUser, error in
                saveUser(loginUser, UIDX: UIDX)
                completion(error: error)
            }
        }
    }

    private static func loginUserToServer(login: String, password: String, completion: (json: AnyObject, UIDX: String) -> Void) {
        let parameters = [
            "login": login,
            "pwd": password
        ]

        Alamofire.request(.POST, "https://www.alza.cz/Services/RestService.svc/v2/loginUser", parameters: parameters, encoding: .JSON)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let headerFields = response.response?.allHeaderFields as? [String: String],
                        let setCookie = headerFields["Set-Cookie"] {
                            var cookieArray = setCookie.componentsSeparatedByString(";")
                            completion(json: JSON, UIDX: cookieArray[0])
                    }
                }
        }
    }

    private static func parseLoginUser(json: AnyObject, completion: (loginUser: LoginUser, error: ErrorArgo) -> Void) {
        if let j: AnyObject = json {
            if let loginUser: LoginUser = decode(j),
                let error: ErrorArgo = decode(j) {
                    completion(loginUser: loginUser, error: error)
            }
        }
    }

    private static func saveUser(loginUser: LoginUser, UIDX: String) {
        let realm = try! Realm()
        let users = realm.objects(UserRealm)
        try! realm.write {
            realm.delete(users)
        }

        let user = UserRealm(name: loginUser.userName, UIDX: UIDX)
        try! realm.write {
            realm.add(user)
        }
    }

    // MARK: Beacons
    static func getBeacons(completion: (beacons: [BeaconArgo], error: ErrorArgo) -> Void) {
        getBeaconsFromServer() { json in
            parseGetBeacons(json) { error in
                if let beacons = error.beacons {
                    completion(beacons: beacons, error: error)
                } else {
                    completion(beacons: [BeaconArgo](), error: error)
                }
            }
        }
    }

    private static func getBeaconsFromServer(completion: (json: AnyObject) -> Void) {
        Alamofire.request(.GET, "https://www.alza.cz/Services/RestService.svc/v1/getBeacons")
            .responseJSON { response in
                if let JSON = response.result.value {
                    completion(json: JSON)
                }
        }
    }

    private static func parseGetBeacons(json: AnyObject, completion: (error: ErrorArgo) -> Void) {
        if let j: AnyObject = json {
            if let error: ErrorArgo = decode(j) {
                completion(error: error)
            }
        }
    }

    // MARK: Detail of Beacon
    static func getBeaconDetail(beaconID: String, completion: (beaconDetail: BeaconArgo, error: ErrorArgo) -> Void) {
        getBeaconDetailFromServer(beaconID) { json in
            parseGetBeaconDetail(json, completion: completion)
        }
    }

    private static func getBeaconDetailFromServer(beaconID: String, completion: (json: AnyObject) -> Void) {
        Alamofire.request(.GET, "https://www.alza.cz/Services/RestService.svc/v1/getBeaconInfo/\(beaconID)")
            .responseJSON { response in
                if let JSON = response.result.value {
                    completion(json: JSON)
                }
        }
    }

    private static func parseGetBeaconDetail(json: AnyObject, completion: (beaconDetail: BeaconArgo, error: ErrorArgo) -> Void) {
        if let j: AnyObject = json {
            if let beacon: BeaconArgo = decode(j),
                let error: ErrorArgo = decode(j) {
                    completion(beaconDetail: beacon, error: error)
            }
        }
    }

    // MARK: Article

    static func getArticle(articleID: String, completion: (article: ArticleArgo, error: ErrorArgo) -> Void) {
        getArticleFromServer(articleID) { json in
            parseGetArticle(json) { article, error in
                if error.id == 0 {
                    saveArticle(article)
                }
                completion(article: article, error: error)
            }
        }
    }

    private static func getArticleFromServer(articleID: String, completion: (json: AnyObject) -> Void) {
        let headers = [
            "Cookie": "\(User.UIDX())"
        ]
        Alamofire.request(.GET, "https://www.alza.cz/Services/RestService.svc/v1/article/\(articleID)", headers: headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    completion(json: JSON)
                }
        }
    }

    private static func parseGetArticle(json: AnyObject, completion: (article: ArticleArgo, error: ErrorArgo) -> Void) {
        if let j: AnyObject = json {
            if let article: ArticleArgo = decode(j),
                let error: ErrorArgo = decode(j) {
                    completion(article: article, error: error)
            }
        }
    }

    private static func saveArticle(article: ArticleArgo) {
        let realm = try! Realm()

        let article = ArticleRealm(content: article.content)
        try! realm.write {
            realm.add(article)
        }
    }

    static func lastArticle() -> ArticleRealm? {
        let realm = try! Realm()
        let articles = realm.objects(ArticleRealm)
        if articles.count > 0 {
            return articles.last
        }
        return nil
    }
    
    // MARK: Order
    static func getOrder(orderID: String, completion: (order: OrderArgo) -> Void) {
        getOrderFromServer(orderID) { json in
            parseGetOrder(json) { order in
                saveOrder(order)
                completion(order: order)
            }
        }
    }
    
    private static func getOrderFromServer(orderID: String, completion: (json: AnyObject) -> Void) {
        let headers = [
            "Cookie": "\(User.UIDX())"
        ]
        Alamofire.request(.GET, "https://www.alza.cz/Services/RestService.svc/v1/getorderdetail/\(orderID)", headers: headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    completion(json: JSON)
                }
        }
    }
    
    private static func parseGetOrder(json: AnyObject, completion: (order: OrderArgo) -> Void) {
        if let j: AnyObject = json {
            let orderJson = j["order"]
            
            if orderJson != nil{
                
                if let order: OrderArgo = decode(orderJson!!) {
                    completion(order: order)
                }
            }
        }
    }
    
    private static func saveOrder(order: OrderArgo) {
        let realm = try! Realm()
        
        let orderItems = List<ItemRealm>()
        
        let orderRealm = OrderRealm(id: order.id, created: order.created, user: order.user, email: order.email, phone: order.phone, address: order.address, deliveryAdress: order.deliveryAdress, deliveryNote: order.deliveryNote, delivery: order.delivery, payment: order.payment, price: order.price, priveVat: order.priceVat, vat: order.vat, message: order.message, messageStatus: order.messageStatus, itemstatusDecription: order.itemstatusDecription, paymentStatusDescription: order.paymentStatusDescription, itemsCnt: order.itemsCnt)
        
        order.items.forEach({(item) in
            let itemRealm = ItemRealm(order: item.order, code: item.code, name: item.name, count: item.count, priceVat: item.priceVat, priceItemVat: item.priceItemVat, img: item.img)
            itemRealm.orderRealm = orderRealm
            orderRealm.items.append(itemRealm)
            
            
//            orderItems.append(itemRealm)
        })
        

        try! realm.write {
            realm.add(orderRealm)
//            realm.add(orderItems)
        }
    }
    
    static func lastOrder() -> OrderRealm? {
        let realm = try! Realm()
        let orders = realm.objects(OrderRealm)
        
        if orders.count > 0 {
            return orders.last
        }
        return nil
    }
    
}