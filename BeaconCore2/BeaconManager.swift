//
//  BeaconManager.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 30.04.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import RealmSwift

class BeaconManager: NSObject, ESTBeaconManagerDelegate, BeaconHistoryRepositoryDelegate, CLLocationManagerDelegate {

	private let beaconManager = ESTBeaconManager()
	private let beaconHistoryRepository = BeaconHistoryRepositoryRealm()
	private let locationManager = CLLocationManager()
	
	private var monitoredBeaconIds: [BeaconID] = []
	private var historyLength: Int = 0
	private var circularRegionBeacons = [String: BeaconID]()

	var actualBeaconRegions: [CLBeaconRegion] = []

	var beaconRegionHistory: [BeaconHistoryEntity] {
		get {
			return Array(beaconHistoryRepository.beaconHistories)
		}
	}

	var delegates: [BeaconManagerDelegate] = []

	convenience init(historyLength: Int) {
		self.init()
		self.historyLength = historyLength
	}

	override init() {
		beaconManager.requestAlwaysAuthorization()
		locationManager.requestAlwaysAuthorization()
		
		super.init()
		
		beaconManager.stopMonitoringForAllRegions()
	
		locationManager.monitoredRegions.forEach({monitoredRegion in
			locationManager.stopMonitoringForRegion(monitoredRegion)
		})
		
		beaconHistoryRepository.delegate = self
		beaconManager.delegate = self
		locationManager.delegate = self
	}

	
	
	func startMonitoring(beaconIds: [BeaconID]) {
		beaconIds.forEach({
			(beaconId) in
			startMonitoring(beaconId)
		})
	}

	func startMonitoring(beaconId: BeaconID) {
		if !monitoredBeaconIds.contains(beaconId) {

			if (beaconId.region == nil) {
				beaconManager.startMonitoringForRegion(beaconId.asBeaconRegion)
			} else {
				circularRegionBeacons[beaconId.asString] = beaconId
				locationManager.startMonitoringForRegion(beaconId.region!)
			}

			monitoredBeaconIds.append(beaconId)
		}
	}
	
	func stopMonitoring(beaconIds: [BeaconID]) {
		beaconIds.forEach({
			(beaconId) in
			stopMonitoring(beaconId)
		})
	}

	func stopMonitoring(beaconId: BeaconID) {
		if monitoredBeaconIds.contains(beaconId) {
			beaconManager.stopMonitoringForRegion(beaconId.asBeaconRegion)
			monitoredBeaconIds.removeAtIndex(monitoredBeaconIds.indexOf(beaconId)!)
		}
	}

	func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion) {
		if !actualBeaconRegions.contains(region) {
			actualBeaconRegions.append(region)
			
			delegates.forEach({(delegate) in
				delegate.beaconManager(manager, didEnterRegion: region)
				delegate.actualBeaconsChanged()
			})
			
		}
	}

	func beaconManager(manager: AnyObject, didExitRegion region: CLBeaconRegion) {
		if (circularRegionBeacons[region.identifier] == nil) {
			exitRegion(manager, didExitRegion: region)
		}
	}

	private func exitRegion(manager: AnyObject, didExitRegion region: CLBeaconRegion) {
		if actualBeaconRegions.contains(region) {
			let index = actualBeaconRegions.indexOf(region)
			actualBeaconRegions.removeAtIndex(index!)
		}

		beaconHistoryRepository.saveBeaconRegionHistory(region)

		delegates.forEach({(delegate) in
			delegate.beaconManager(manager, didExitRegion: region)
			delegate.actualBeaconsChanged()
		})
		
	}

	func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
		if (!(region is CLBeaconRegion) && circularRegionBeacons[region.identifier] != nil) {
			beaconManager.startMonitoringForRegion(circularRegionBeacons[region.identifier]!.asBeaconRegion)
		}
	}

	func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
		if (!(region is CLBeaconRegion) && circularRegionBeacons[region.identifier] != nil) {
			let beaconId = circularRegionBeacons[region.identifier]!
			beaconManager.stopMonitoringForRegion(beaconId.asBeaconRegion)
			
			if actualBeaconRegions.contains(beaconId.asBeaconRegion) {
				exitRegion(manager, didExitRegion: circularRegionBeacons[region.identifier]!.asBeaconRegion)
			}
		}
	}
	
	private func deleteOverHistoryLength() {
		if (beaconRegionHistory.count > historyLength) {
			for index in 1 ... (beaconRegionHistory.count - historyLength) {
				beaconHistoryRepository.delete(beaconHistoryRepository.beaconHistories[index - 1])

			}
		}
	}

	func deleteAllHistory() {
		beaconHistoryRepository.deleteAll()
	}

	func recordChanged(inserted: [Int : BeaconHistoryEntity], updated: [Int : BeaconHistoryEntity], deletedIndexes: [Int]) {
		if (inserted.count > 0) {
			deleteOverHistoryLength()
		}
		
		delegates.forEach({(delegate) in
			delegate.beaconHistoryChanged(inserted, updated: updated, deletedIndexes: deletedIndexes)
		})
	}

}

protocol BeaconManagerDelegate {
	func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion)
	func beaconManager(manager: AnyObject, didExitRegion region: CLBeaconRegion)

	func actualBeaconsChanged()
	func beaconHistoryChanged(inserted: [Int : BeaconHistoryEntity], updated: [Int : BeaconHistoryEntity], deletedIndexes: [Int])
}

extension BeaconManagerDelegate {
	func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion) { }
	func beaconManager(manager: AnyObject, didExitRegion region: CLBeaconRegion) { }

	func actualBeaconsChanged() { }
	func beaconHistoryChanged(inserted: [Int : BeaconHistoryEntity], updated: [Int : BeaconHistoryEntity], deletedIndexes: [Int]) { }
}
