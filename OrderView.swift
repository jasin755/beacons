//
//  OrdersView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 27.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderView : UIView{
    
    var orderId : String?{
        didSet{
            if let id = orderId{
                loadOrder(id)
            }
        }
    }
    
    private let orderNumberBox = OrderNumberBoxView()
    private let orderDetailBox = OrderDetailBoxView()
    private let orderItemView = OrderItemsView()
    private let orderSummaryView = OrderSummaryView()
    private let itemsHeader = ItemsHeaderView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        addSubview(orderNumberBox)
        addSubview(orderDetailBox)
        addSubview(itemsHeader)
        addSubview(orderItemView)
        addSubview(orderSummaryView)
        
        orderNumberBox.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(50)
        })
        
        orderDetailBox.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self)
            make.top.equalTo(orderNumberBox.snp_bottom)
            make.right.equalTo(self)
            make.height.equalTo(120)
        })
        
        
        itemsHeader.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.top.equalTo(orderDetailBox.snp_bottom)
            make.height.equalTo(40)
        })
        
        orderItemView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.top.equalTo(itemsHeader.snp_bottom)
            make.height.equalTo(200)
        })
        
        orderSummaryView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self)
            make.top.equalTo(orderItemView.snp_bottom)
            make.right.equalTo(self)
            make.height.equalTo(90)
        })
        
    }
    
    
    func setOrder(order : OrderRealm){
        orderNumberBox.orderNumber = order["id"] as! String?
        
        orderDetailBox.paymentType = order["payment"] as! String?
        orderDetailBox.dateCreated = order["created"] as! String?
        orderDetailBox.email = order["email"] as! String?
        orderDetailBox.phone = order["phone"] as! String?
        
        var itemsArgo : [ItemArgo] = []
        
        order.items.forEach({(item) in
            let itemArgo = ItemArgo(order: item.order, code: item.code, name: item.name, count: item.count, priceVat: item.priceVat, priceItemVat: item.priceItemVat, img: item.img)
            
            itemsArgo.append(itemArgo)
        
        })
        
        orderItemView.items = itemsArgo
        
        orderSummaryView.price = order["price"] as! String?
        orderSummaryView.vat = order["vat"] as! String?
        orderSummaryView.priceVat = order["priveVat"] as! String?
    }
    
    func loadOrder(orderId : String){
        NetworkManager.getOrder(orderId, completion: {[weak self] (order) in
            self!.orderNumberBox.orderNumber = order.id
            
            self!.orderDetailBox.paymentType = order.payment
            self!.orderDetailBox.dateCreated = order.created
            self!.orderDetailBox.email = order.email
            self!.orderDetailBox.phone = order.phone
            
            self!.orderItemView.items = order.items
            
            self!.orderSummaryView.price = order.price
            self!.orderSummaryView.vat = order.vat
            self!.orderSummaryView.priceVat = order.priceVat
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

