//
//  OrderSummaryView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 28.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderSummaryView : UIView{

    private var priceLabel = UILabel()
    private var vatLabel = UILabel()
    private var priceVatLabel = UILabel()
    
    private var priceValueLabel : UILabel!
    private var vatValueLabel : UILabel!
    private var priceVatValueLabel : UILabel!
    
    var price : String?{
        didSet{
            if let p = price{
                priceValueLabel.text = p
            }
        }
    }
    
    var vat : String?{
        didSet{
            if let v = vat{
                vatValueLabel.text = v
            }
        }
    }
    
    var priceVat : String?{
        didSet{
            if let pV = priceVat{
                priceVatValueLabel.text = pV
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        priceLabel = createLabel("Cena bez DPH")
        vatLabel = createLabel("DPH")
        priceVatLabel = createLabel("K úhradě")
        priceVatLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        
        priceValueLabel = createLabel(nil)
        vatValueLabel = createLabel(nil)
        
        priceVatValueLabel = createLabel(nil)
        priceVatValueLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        priceVatValueLabel.textColor = UIColor(red: 141/255, green: 183/255, blue: 23/255, alpha: 1)
        
        priceValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(priceLabel)
            make.right.equalTo(self).inset(10)
        })
    
        priceLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(self).offset(5)
            make.left.equalTo(self).offset(80)
        })
        
        
        vatLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(priceLabel.snp_bottom)
            make.right.equalTo(priceLabel)
        })
        
        vatValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(vatLabel)
            make.right.equalTo(priceValueLabel)
        })
        
        priceVatLabel.snp_makeConstraints(closure: {(make) in
            make.bottom.equalTo(self).offset(5)
            make.right.equalTo(vatLabel)
        })
        
        priceVatValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(priceVatLabel)
            make.right.equalTo(vatValueLabel)
        })
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}