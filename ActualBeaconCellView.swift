//
//  BeaconHistoryCellView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class ActualBeaconCellView : UITableViewCell{
    
    var uuidValueLabel : UILabel!
    var majorValueLabel : UILabel!
    var minorValueLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.None
        
        
        let uuidLabel = createLabel("UUID:")
        let majorLabel = createLabel("Major:")
        let minorLabel = createLabel("Minor:")
        
        uuidValueLabel = createLabel(nil)
        majorValueLabel = createLabel(nil)
        minorValueLabel = createLabel(nil)
        
     
        addDefaultBorder()
        
        contentView.addSubview(uuidValueLabel)
        contentView.addSubview(majorValueLabel)
        contentView.addSubview(minorValueLabel)
        
    
        uuidLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(contentView).offset(3)
            make.left.equalTo(contentView).offset(5)
        })
        
        uuidValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(uuidLabel)
            make.left.equalTo(uuidLabel.snp_right).offset(5)
        })
        
        majorLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(uuidLabel.snp_bottom).offset(3)
            make.left.equalTo(uuidLabel)
        })
        
        majorValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(majorLabel)
            make.left.equalTo(majorLabel.snp_right).offset(5)
        })
        
        minorLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(majorLabel.snp_bottom).offset(3)
            make.left.equalTo(majorLabel)
        })
        
        minorValueLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(minorLabel)
            make.left.equalTo(majorLabel.snp_right).offset(5)
        })
       
        
    }
    
  
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
