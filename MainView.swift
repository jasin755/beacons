//
//  MainView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class MainView : UIView{
    
    var menuButtonClick : ((sender : UIButton) -> Void)?
    var contentView : UIView? {
        willSet{
            if let cV = contentView{
                cV.removeFromSuperview()
            }
        }
        
        didSet{
            showContent()
        }
        
    }
    
    private var topView : TopView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .whiteColor()
        //layer.shadowColor = UIColor(red: 156/255, green: 224/255, blue: 248/255, alpha: 1).CGColor
        layer.shadowColor = UIColor(red: 202/255, green: 240/255, blue: 255/255, alpha: 1).CGColor
        layer.shadowRadius = CGFloat(100)
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSizeZero
        layer.masksToBounds = false
        
        topView = TopView()
        addSubview(topView)
        
        topView.snp_makeConstraints(closure: {[weak self] (make) in
            make.top.equalTo(self!)
            make.left.equalTo(self!)
            make.right.equalTo(self!)
            make.height.equalTo(80)
        })
        
    
        topView.menuButtonClick = { [weak self] (sender) in
            if let clickFuncion = self!.menuButtonClick {
               clickFuncion(sender: sender)
            }
        }
        
    }
    
    private func showContent(){
        
        if let cV = contentView{
            
            addSubview(cV)
            
            cV.snp_makeConstraints(closure: {[weak self] (make) in
                make.top.equalTo(self!.topView.snp_bottom)
                make.left.equalTo(self!)
                make.right.equalTo(self!)
                make.bottom.equalTo(self!)
            })
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}