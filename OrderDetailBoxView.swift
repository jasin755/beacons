//
//  OrderDetailBoxView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 28.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderDetailBoxView : UIView{

    
    
    var paymentType : String?{
        didSet{
            if let type = paymentType{
                paymentTypeValueLabel.text = type
            }
        }
    }
    
    var dateCreated : String?{
        didSet{
            if let date = self.dateCreated{
                dateCreatedValueLabel.text = date
            }
        }
    }
    
    var email : String?{
        didSet{
            if let mail = email{
                emailValueLabel.text = mail
            }
        }
    }
    
    var phone : String?{
        didSet{
            if let tel = phone{
                phoneValueLabel.text = tel
            }
        }
    }
    
    private let paymentTypeValueLabel = UILabel()
    private let dateCreatedValueLabel = UILabel()
    private let emailValueLabel = UILabel()
    private let phoneValueLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        let paymentTypeLabel = UILabel()
        paymentTypeLabel.text = "Způsob úhrady:"
        paymentTypeLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        paymentTypeLabel.textColor = Config.defaultFontColor
        
        
        paymentTypeValueLabel.textColor = Config.defaultFontColor
        paymentTypeValueLabel.font = Config.defaultFont
        
        let dateCreatedLabel = UILabel()
        dateCreatedLabel.text = "Datum:"
        dateCreatedLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        dateCreatedLabel.textColor = Config.defaultFontColor
        
        
        dateCreatedValueLabel.font = Config.defaultFont
        dateCreatedValueLabel.textColor = Config.defaultFontColor
        
        
        let emailLabel = UILabel()
        emailLabel.text = "Email:"
        emailLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        emailLabel.textColor = Config.defaultFontColor
        
        
        emailValueLabel.font = Config.defaultFont
        emailValueLabel.textColor = Config.defaultFontColor
        
        
        let phoneLabel = UILabel()
        phoneLabel.text = "Telefon:"
        phoneLabel.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        phoneLabel.textColor = Config.defaultFontColor
        
        
        phoneValueLabel.font = Config.defaultFont
        phoneValueLabel.textColor = Config.defaultFontColor
        
        addSubview(paymentTypeLabel)
        addSubview(paymentTypeValueLabel)
        addSubview(dateCreatedLabel)
        addSubview(dateCreatedValueLabel)
        addSubview(emailLabel)
        addSubview(emailValueLabel)
        addSubview(phoneLabel)
        addSubview(phoneValueLabel)
        
        paymentTypeLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self).offset(5)
            make.top.equalTo(self).offset(8)
        })
        
        paymentTypeValueLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(paymentTypeLabel.snp_right).offset(3)
            make.centerY.equalTo(paymentTypeLabel)
        })
        
        dateCreatedLabel.snp_makeConstraints(closure: {(make) in
            make.top.equalTo(paymentTypeLabel.snp_bottom).offset(10)
            make.left.equalTo(paymentTypeLabel)
        })
        
        dateCreatedValueLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(dateCreatedLabel.snp_right).offset(3)
            make.centerY.equalTo(dateCreatedLabel)
        })
        
        emailLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(dateCreatedLabel)
            make.top.equalTo(dateCreatedLabel.snp_bottom).offset(10)
        })
        
        emailValueLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(emailLabel.snp_right).offset(3)
            make.centerY.equalTo(emailLabel)
        })
        
        phoneLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(emailLabel)
            make.top.equalTo(emailLabel.snp_bottom).offset(10)
        })
        
        phoneValueLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(phoneLabel.snp_right).offset(3)
            make.centerY.equalTo(phoneLabel)
        })

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}