//
//  TopView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 23.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class TopView : UIView{

    var menuButtonClick : ((sender : UIButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 194/255, green: 235/255, blue: 249/255, alpha: 1)
        
        let imageViewLogo = UIImageView(image: UIImage(named: "alza_logo"))
        let buttonMenu = UIButton()
        
        buttonMenu.setImage(UIImage(named: "alza_hamburger_menu"), forState: .Normal)
        buttonMenu.contentMode = UIViewContentMode.ScaleAspectFit
        
        addSubview(imageViewLogo)
        addSubview(buttonMenu)
        
        imageViewLogo.snp_makeConstraints(closure: {[weak self] (make) in
            make.centerX.equalTo(self!)
            make.top.equalTo(25)
        })
        
        buttonMenu.snp_makeConstraints(closure: {[weak self] (make) in
            make.left.equalTo(self!).offset(-20)
            make.centerY.equalTo(imageViewLogo)
            make.width.equalTo(100)
            make.height.equalTo(60)
        })
        
        buttonMenu.addTarget(self, action: #selector(menuButtonClicked), forControlEvents: UIControlEvents.TouchUpInside)
        
    }
    
    func menuButtonClicked(sender:UIButton!){
        if let clickFunction = menuButtonClick{
            clickFunction(sender: sender)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
