//
//  ArticleView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 25.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class ArticleView : UIWebView{
    
    
    var articleId : Int?{
        didSet{
            loadContent(articleId!)
        }
    }
    
    var htmlContent : String?{
        didSet{
            if let html = htmlContent{
                loadHTMLString(html, baseURL: nil)
            }
        }
    }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    private func loadContent(articleId : Int){
        
        
        NetworkManager.getArticle("\(articleId)", completion: {[weak self] (article, error) in
            if error.id == 0{
                self!.htmlContent = article.content
            }else{
                if let message = error.message{
                    print(message)
                }else{
                    print("Nastala chyba pri nacitani clanku s ID \(articleId)")
                }
            }
        })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}