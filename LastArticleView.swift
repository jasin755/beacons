//
//  LastArticleView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 25.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class LastArticleView : ArticleView{

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let article = NetworkManager.lastArticle()
        
        if let a = article{
            htmlContent = a["content"] as! String?
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}