//
//  HomePageView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class HomePageView : UITableView, UITableViewDataSource, BeaconManagerDelegate{
    
    var beaconManager : BeaconManager? {
        didSet{
            loadActualBeacons()
        }
    }
    
    private let cellId = "actualBeaconsCellId"
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        dataSource = self
        registerClass(ActualBeaconCellView.self, forCellReuseIdentifier: cellId)
        rowHeight = 60
        separatorStyle = UITableViewCellSeparatorStyle.None
        backgroundColor = UIColor.clearColor()
    }
    
    
    private func loadActualBeacons(){
        beaconManager!.delegates.append(self)
    }
    
    func actualBeaconsChanged() {
        dispatch_async(dispatch_get_main_queue(), { [weak self] in
            self!.reloadData()
        })
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! ActualBeaconCellView
        
        let actualBeacon = beaconManager!.actualBeaconRegions[indexPath.row]
        
        cell.uuidValueLabel.text = actualBeacon.proximityUUID.UUIDString
        
        var major : String
        var minor : String
        
        if let maj = actualBeacon.major{
            major = "\(maj)"
        }else{
            major = "Neznámý"
        }
        
        if let min = actualBeacon.minor{
            minor = "\(min)"
        }else{
            minor = "Neznámy"
        }
        
        
        cell.majorValueLabel.text = major
        cell.minorValueLabel.text = minor
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        if let bM = beaconManager{
            numberOfRows = bM.actualBeaconRegions.count
        }
        
        if numberOfRows < 1 {
            let noDataView = UIImageView(image: UIImage(named: "alza_beacons_not_found"))
            noDataView.contentMode = .ScaleAspectFit
            
            backgroundView = noDataView
            
            noDataView.snp_makeConstraints(closure: {[weak self] (make) in
                make.height.equalTo(450)
                make.width.equalTo(450)
                make.center.equalTo(self!)
            })
            
            scrollEnabled = false
            
        }else{
            backgroundView = nil
            scrollEnabled = true
        }
        
        return numberOfRows
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
}