//
//  BeaconHistoryView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class BeaconHistoryView : UITableView, BeaconManagerDelegate, UITableViewDataSource{
    
    var beaconManager : BeaconManager?{
        didSet{
            loadBeaconHistory()
        }
    }
    
    private let cellId = "beaconHistoryCellId"
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        dataSource = self
        registerClass(BeaconHistoryCellView.self, forCellReuseIdentifier: cellId)
        rowHeight = 85
        separatorStyle = UITableViewCellSeparatorStyle.None
        backgroundColor = UIColor.clearColor()
    }
    
    
    private func loadBeaconHistory(){
        beaconManager!.delegates.append(self)
        reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! BeaconHistoryCellView
        let beaconRegionHistory = Array(beaconManager!.beaconRegionHistory.reverse())[indexPath.row]
        
        let dateFormater = NSDateFormatter()
        
        dateFormater.dateFormat = "hh:mm:ss dd.MM.yyyy"
        
        cell.uuidValueLabel.text = beaconRegionHistory.uuid
        cell.majorValueLabel.text = "\(beaconRegionHistory.major)"
        cell.minorValueLabel.text = "\(beaconRegionHistory.minor)"
        cell.dateValueLabel.text = dateFormater.stringFromDate(beaconRegionHistory.date)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 0
        
        if let bM = beaconManager{
            numberOfRows = bM.beaconRegionHistory.count
        }
        
        return numberOfRows
    }
    
    func beaconHistoryChanged(inserted: [Int : BeaconHistoryEntity], updated: [Int : BeaconHistoryEntity], deletedIndexes: [Int]) {
        dispatch_async(dispatch_get_main_queue(), { [weak self] in
            self!.reloadData()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}