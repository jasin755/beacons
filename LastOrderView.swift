//
//  LastOrderView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 28.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class LastOrderView : OrderView{

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let lastOrder = NetworkManager.lastOrder()
        
        if let order = lastOrder{
            setOrder(order)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}