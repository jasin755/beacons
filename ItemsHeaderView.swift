//
//  ItemsHeaderView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 28.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class ItemsHeaderView : UIView {

    private let summaryLabel = UILabel()
    private let quantityLabel = UILabel()
    private let productLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 236/255, green: 245/255, blue: 254/255, alpha: 1)
        
        let font = UIFont(name: "NimbusSanL-Bol", size: 16)
        
        summaryLabel.text = "Celkem"
        summaryLabel.font = font
        
        quantityLabel.text = "Ks"
        quantityLabel.font = font
        
        productLabel.text = "Produkt"
        productLabel.font = font
        
        addSubview(quantityLabel)
        addSubview(summaryLabel)
        addSubview(productLabel)
        
        summaryLabel.snp_makeConstraints(closure: {(make) in
            make.centerY.equalTo(self)
            make.right.equalTo(self).inset(15)
        })
        
        quantityLabel.snp_makeConstraints(closure: {(make) in
            make.centerY.equalTo(self)
            make.right.equalTo(summaryLabel.snp_left).offset(-70)
        })
        
        productLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(self).offset(10)
            make.centerY.equalTo(self)
        })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}