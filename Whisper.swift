//
//  Whisper.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 27.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class Whisper{
    
    private let mainView : UIView = UIView()
    private var onClick : (() -> Void)?
    
    init(title : String, subTitle : String?, controller : UIViewController, onClick : (() -> Void)?){
        
        mainView.backgroundColor = .whiteColor()
        controller.view.addSubview(mainView)
        
        self.onClick = onClick
        
        let imageView = UIImageView(image: UIImage(named: "alza_whisper_icon"))
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        mainView.addSubview(imageView)
        
        let titleLabel = mainView.createLabel(title)
        
        let border = UIView()
        border.backgroundColor = UIColor(red: 225/250, green: 225/250, blue: 225/250, alpha: 1)
        mainView.addSubview(border)
        
        mainView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(controller.view)
            make.top.equalTo(controller.view)
            make.right.equalTo(controller.view)
            make.height.equalTo(90)
        })
        
        
        imageView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(mainView)
            make.top.equalTo(mainView).offset(20)
            make.height.equalTo(60)
            make.width.equalTo(60)
        })
        
        titleLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(imageView.snp_right).offset(5)
            make.top.equalTo(imageView).offset(10)
        })
        
        border.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(mainView)
            make.right.equalTo(mainView)
            make.bottom.equalTo(mainView)
            make.height.equalTo(3)
        })
        
        if let subT = subTitle{
            let subTitleLabel = mainView.createLabel(subT)
            
            subTitleLabel.snp_makeConstraints(closure: {(make) in
                make.top.equalTo(titleLabel).offset(20)
                make.left.equalTo(titleLabel)
            })
        }
        
        UIView.animateWithDuration(0.4, animations: { [weak self] in
            //Trosku hack ale funguje
            self!.mainView.frame.origin.y += 90
        })
        
        mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(click)))
        
        NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(hideAnnouncement), userInfo: ["mainView" : mainView], repeats: false)

    }


    @objc func click(){
        mainView.hidden = true
        mainView.removeFromSuperview()
        if let closure = onClick{
            closure()
        }
    }
    
    @objc func hideAnnouncement(timer : NSTimer){
        let mainView = timer.userInfo!["mainView"] as! UIView
        
        UIView.animateWithDuration(0.4, animations: {
            mainView.frame.origin.y -= 90
            }, completion: { (finished) in
                mainView.removeFromSuperview()
        })
        
        
    }
    
}