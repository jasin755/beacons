//
//  MenuItemType.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation

enum MenuItemType : Int {
    case HomePage
    case LastOrder
    case BeaconHistory
    case LastLarticle
}