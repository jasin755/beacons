//
//  Article.swift
//  BeaconCore2
//
//  Created by Jakub Lares on 22.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry
import RealmSwift

struct ArticleArgo {
    let content: String
}

extension ArticleArgo: Decodable {
    static func decode(j: JSON) -> Decoded<ArticleArgo> {
        return curry(ArticleArgo.init)
        <^> j <| "content"
    }
}

class ArticleRealm: Object {
    var content: String = ""

    convenience init(content: String) {
        self.init()
        self.content = content
    }
}

