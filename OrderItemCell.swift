//
//  OrderItemCell.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 27.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class OrderItemCell : UITableViewCell{

        
    private var itemImageView : UIImageView!
    private var itemNameLabel : UILabel!
    private var quantityLabel : UILabel!
    private var priceLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        itemImageView = UIImageView()
        itemImageView.contentMode = UIViewContentMode.ScaleAspectFit
        contentView.addSubview(itemImageView)
        
        itemImageView.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(contentView).offset(5)
            make.top.equalTo(contentView).offset(5)
            make.bottom.equalTo(contentView).offset(5)
            make.width.equalTo(70)
        })
        
        itemNameLabel = UILabel()
        itemNameLabel.font = Config.defaultFont
        
        quantityLabel = createLabel(nil)
        priceLabel = createLabel(nil)
        
        contentView.addSubview(itemNameLabel)
        contentView.addSubview(quantityLabel)
        contentView.addSubview(priceLabel)
        
        itemNameLabel.snp_makeConstraints(closure: {(make) in
            make.left.equalTo(itemImageView.snp_right).offset(4)
            make.right.equalTo(contentView)
            make.top.equalTo(contentView).offset(10)
        })
        
        priceLabel.snp_makeConstraints(closure: {(make) in
            make.right.equalTo(contentView).inset(25)
            make.centerY.equalTo(contentView).offset(5)
        })
        
        quantityLabel.snp_makeConstraints(closure: {(make) in
            make.centerY.equalTo(contentView).offset(5)
            make.right.equalTo(priceLabel.snp_left).offset(-85)
        })
        
    }
    
    
    func setContent(imageUrl : String?, itemName : String, itemCount : Int, price : String){
        itemNameLabel.text = itemName
        quantityLabel.text = "\(itemCount)"
        priceLabel.text = price
        
        if let url = imageUrl{
            downloadImage(url)
        }
        
    }
    
    private func downloadImage(url : String){
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!) {[weak self] (data, response, error) in
            dispatch_async(dispatch_get_main_queue()) {
                self!.itemImageView!.image = UIImage(data: data!)
            }
        }.resume()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}