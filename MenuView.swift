//
//  MenuView.swift
//  BeaconCore2
//
//  Created by Nikolaj Pognerebko on 24.05.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MenuView : UIView, UITableViewDelegate, UITableViewDataSource{

    private var buttons : [UIButton] = []
    private let cellId = "mainMenuCellId"
    
    var menuClicked : ((menuItemType : MenuItemType) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let imageViewLogo = UIImageView(image: UIImage(named: "alza_logo"))
        let tableViewButtons = UITableView()
        
        tableViewButtons.scrollEnabled = false
        tableViewButtons.separatorStyle = UITableViewCellSeparatorStyle.None
        tableViewButtons.backgroundColor = UIColor.clearColor()
        tableViewButtons.delegate = self
        tableViewButtons.dataSource = self
        tableViewButtons.registerClass(MenuItemView.self, forCellReuseIdentifier: cellId)
        
        addSubview(imageViewLogo)
        addSubview(tableViewButtons)
        
        imageViewLogo.snp_makeConstraints(closure: {[weak self] (make) in
            make.top.equalTo(self!).inset(30)
            make.left.equalTo(self!).inset(30)
        })
        
        tableViewButtons.snp_makeConstraints(closure: {[weak self] (make) in
            make.left.equalTo(self!)
            make.right.equalTo(self!)
            make.top.equalTo(imageViewLogo.snp_bottom).offset(5)
            make.bottom.equalTo(self!)
        })
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowsCount = 0
        
        switch section {
        case 0:
            rowsCount = 4
            break;
        case 1:
            rowsCount = 0
            break;
        default:
            print("Chyba")
        }
        
        return rowsCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! MenuItemView
    
        
        switch indexPath.row {
        case MenuItemType.HomePage.rawValue:
            cell.title = "Úvod"
            cell.buttonImage = UIImage(named: "alza_home_icon")
            cell.type = MenuItemType.HomePage
            break
        case MenuItemType.LastOrder.rawValue:
            cell.title = "Poslední objednávka"
            cell.buttonImage = UIImage(named: "alza_share_icon")
            cell.type = MenuItemType.LastOrder
            break
        case MenuItemType.BeaconHistory.rawValue:
            cell.title = "Historie beaconu"
            cell.buttonImage = UIImage(named: "alza_share_icon")
            cell.type = MenuItemType.BeaconHistory
            break;
        case MenuItemType.LastLarticle.rawValue:
            cell.title = "Poslední článek"
            cell.buttonImage = UIImage(named: "alza_home_icon")
            cell.type = MenuItemType.LastLarticle
            break;
        default:
            print("Chyba nenelezeny MenuItemType \(indexPath.row)")
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell!.contentView.backgroundColor = UIColor(red: 233/255, green: 248/255, blue: 255/255, alpha: 1)
        
    
        if let method = menuClicked{
            let menuItemType = MenuItemType(rawValue: indexPath.row)
            
            if let type = menuItemType{
                method(menuItemType: type)
            }else{
                print("Nenalezeny MenuItemType \(indexPath.row)")
            }
        }
        
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.contentView.backgroundColor = UIColor.clearColor()
    }
    
}


